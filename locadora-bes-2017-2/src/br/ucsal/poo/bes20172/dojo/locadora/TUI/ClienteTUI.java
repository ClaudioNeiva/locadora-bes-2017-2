package br.ucsal.poo.bes20172.dojo.locadora.TUI;

import java.util.List;
import java.util.Scanner;

import br.ucsal.poo.bes20172.dojo.locadora.business.ClienteBO;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Cliente;

public class ClienteTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void menu() {

		Integer opcao;
		do {
			opcao = 0;
			sc = new Scanner(System.in);
			System.out.println("=======================================");
			System.out.println("           Gerenciar Clientes          ");
			System.out.println("=======================================");
			System.out.println("1 - Cadastrar Cliente");
			System.out.println("2 - Remover Cliente");
			System.out.println("3 - Atualizar Cliente");
			System.out.println("4 - Imprimir Clientes");
			System.out.println("5 - Voltar");
			System.out.println("---------------------------------------");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 5) {
				System.out.println(" Opão inválida");
			} else {
				navegarOpcoes(opcao);
			}

		} while (opcao != 5);

	}

	private static void navegarOpcoes(Integer opcao) {
		switch (opcao) {
		case 1:
			try {
				cadastrarCliente();
			} catch (Exception e) {
			}
			break;
		case 2:
			try {
				selecionarClienteERemover();
			} catch (Exception e) {
			}
			break;
		case 3:
			try {
				selecionarClienteEAtualizar();
			} catch (Exception e) {
			}
			break;
		case 4:
			try {
				imprimirClientes();
			} catch (Exception e) {
			}
			break;
		default:
			break;
		}
	}

	public static void imprimirClientes() throws Exception {
		List<Cliente> clientes;
		try {
			clientes = ClienteBO.findAll();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception();
		}
		System.out.println("Os clientes Cadastrados são:");
		listarClientes(clientes);
	}

	private static void listarClientes(List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			System.out.println("---------------------------------------");
			System.out.println("  Nome: " + cliente.getNome());
			System.out.println("  Cpf: " + cliente.getCpf());
			System.out.println("  Endereço: " + cliente.getEndereco());
			System.out.println("---------------------------------------");
		}
	}

	private static void selecionarClienteEAtualizar() throws Exception {
		imprimirClientes();
		Cliente cliente = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("===============================================");
			System.out.println(" Selecione um cliente pelo CPF para atualizar: ");
			System.out.print(" > ");
			String cpf = sc.nextLine();
			try {
				cliente = ClienteBO.findByCPF(cpf);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		atualizarCliente(cliente);
	}

	private static void atualizarCliente(Cliente cliente) {
		Cliente cienteAtualizado = camposCadastroCliente(cliente);
		try {
			ClienteBO.update(cienteAtualizado);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Atualizado com Sucesso");
	}

	private static void selecionarClienteERemover() throws Exception {
		imprimirClientes();
		Cliente cliente = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("===============================================");
			System.out.println(" Selecione um cliente pelo CPF para remover: ");
			System.out.print(" > ");
			String cpf = sc.nextLine();
			try {
				cliente = ClienteBO.findByCPF(cpf);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		ClienteBO.delete(cliente);
		System.out.println("Removido com sucesso");
	}

	private static void cadastrarCliente() throws Exception {
		boolean retorno;
		do {
			retorno = false;
			Cliente cliente = new Cliente();
			cliente = camposCadastroCliente(cliente);
			try {
				ClienteBO.inserir(cliente);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println("-----------------------------------");
				System.out.println(" Digite novamente as Informações ");
				System.out.println("-----------------------------------");
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		System.out.println("Criado com sucesso");
	}

	private static void continuarOuNao() throws Exception {
		Integer opcao = 0;
		do {
			System.out.println("1 - Continuar");
			System.out.println("2 - Voltar");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 2) {
				System.out.println(" Opão inválida");
				opcao = -1;
			}

		} while (opcao == -1);
		if (opcao == 2) {
			throw new Exception();
		}
	}

	private static Cliente camposCadastroCliente(Cliente cliente) {
		sc = new Scanner(System.in);
		System.out.print("Digite o nome: ");
		cliente.setNome(sc.nextLine());
		System.out.print("Digite o CPF: ");
		cliente.setCpf(sc.nextLine());
		System.out.print("Digite o Endereço: ");
		cliente.setEndereco(sc.nextLine());
		return cliente;
	}

}
