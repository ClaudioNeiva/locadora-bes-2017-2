package br.ucsal.poo.bes20172.dojo.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Veiculo;

public class VeiculoDAO {
	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();

	public static List<Veiculo> findAll() {
		return veiculos;
	}

	public static void insert(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static void delete(Veiculo veiculo) {
		veiculos.remove(veiculo);
	}

	public static void update(Veiculo veiculo) {
		/** Atualiza Automaticamente por contada Instância **/
	}

	public static Veiculo findByVeiculo(Veiculo veiculo) {
		if (veiculos.contains(veiculo)) {
			return veiculo;
		}
		return null;
	}

	public static Veiculo findByPlaca(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
	}
}
