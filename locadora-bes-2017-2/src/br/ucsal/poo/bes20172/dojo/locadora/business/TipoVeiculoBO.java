package br.ucsal.poo.bes20172.dojo.locadora.business;

import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.TipoVeiculo;
import br.ucsal.poo.bes20172.dojo.locadora.persistence.TipoVeiculoDAO;

public class TipoVeiculoBO {
	public static List<TipoVeiculo> findAll() {
		return TipoVeiculoDAO.findAll();
	}
}
