package br.ucsal.poo.bes20172.dojo.locadora.business;

import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Cliente;
import br.ucsal.poo.bes20172.dojo.locadora.persistence.ClienteDAO;

public class ClienteBO {

	static Boolean UPDATE = false;
	static Boolean CREATE = true;

	public static void inserir(Cliente cliente) throws Exception {
		validarCampos(cliente, CREATE);
		ClienteDAO.insert(cliente);
	}

	public static void delete(Cliente cliente){
		ClienteDAO.delete(cliente);
	}

	public static void update(Cliente cliente) throws Exception {
		validarCampos(cliente, UPDATE);
		ClienteDAO.update(cliente);
	}

	public static List<Cliente> findAll() throws Exception {
		List<Cliente> clientes = ClienteDAO.findAll();
		if (clientes.isEmpty()) {
			throw new Exception("Ainda não possui clientes cadastrados");
		}
		return clientes;
	}

	private static void validarCampos(Cliente cliente, Boolean operacao) throws Exception {
		if (cliente.getNome() == null) {
			throw new Exception("Erro: Nome inexistente");
		}
		if (cliente.getCpf() == null) {
			throw new Exception("Erro: CPF inexistente");
		}
		String cpf = cliente.getCpf().replace(".", "");
		verificarNumerosCPF(cpf);
		if (cpf.length() != 11) {
			throw new Exception("Erro: CPF inválido, só é permitido CPF com 11 números");
		}
		verificarCpfRepetido(cliente, operacao);
		if (cliente.getEndereco() == null) {
			throw new Exception("Erro: Endereço inexistente");
		}
	}

	private static void verificarNumerosCPF(String cpf) throws Exception {
		try {
			Float.parseFloat(cpf);
		} catch (Exception e) {
			throw new Exception("Erro: CPF inválido, só é permitido CPF com 11 números");
		}
	}

	private static void verificarCpfRepetido(Cliente cliente, Boolean operacao) throws Exception {
		List<Cliente> clientes = ClienteDAO.findAll();
		int cont = (operacao) ? 1 : 0; // 0 para UPDATE e 1 para CREATE
		for (Cliente clienteLista : clientes) {
			if (clienteLista.getCpf().equals(cliente.getCpf())) {
				cont++;
				if (cont == 2)
					throw new Exception("CPF já existe");
			}
		}

	}

	public static Cliente findByCPF(String cpf) throws Exception {
		verificarNumerosCPF(cpf);
		Cliente cliente = ClienteDAO.findByCPF(cpf);
		if (cliente == null) {
			throw new Exception("Cliente inexistente");
		}
		return cliente;
	}
}
