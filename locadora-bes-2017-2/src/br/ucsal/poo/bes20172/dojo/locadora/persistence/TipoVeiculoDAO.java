package br.ucsal.poo.bes20172.dojo.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.TipoVeiculo;

public class TipoVeiculoDAO {
	private static List<TipoVeiculo> tipoVeiculos = new ArrayList<TipoVeiculo>();

	public static List<TipoVeiculo> findAll() {
		return tipoVeiculos;
	}
}
