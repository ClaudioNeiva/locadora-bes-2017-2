package br.ucsal.poo.bes20172.dojo.locadora.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Contrato;
import br.ucsal.poo.bes20172.dojo.locadora.domain.TipoVeiculo;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Veiculo;
import br.ucsal.poo.bes20172.dojo.locadora.enums.ETipo;
import br.ucsal.poo.bes20172.dojo.locadora.persistence.ContratoDAO;

public class ContratoBO {

	public static void inserir(Contrato contrato) throws Exception {
		validarCampos(contrato);
		contrato.setValorTotal(somarValorTotal(contrato));
		ContratoDAO.insert(contrato);
	}

	public static void delete(Contrato contrato) {
		ContratoDAO.delete(contrato);
	}

	public static void update(Contrato contrato) {
		ContratoDAO.update(contrato);
	}

	public static List<Contrato> findByClientesOrdenado() throws Exception {
		List<Contrato> contratos = ContratoDAO.findByClientesOrdenado();
		if (contratos.isEmpty()) {
			throw new Exception("Ainda não possui contratos cadastrados");
		}
		return contratos;
	}

	public static List<Contrato> findAll() throws Exception {
		List<Contrato> contratos = ContratoDAO.findAll();
		if (contratos.isEmpty()) {
			throw new Exception("Ainda não possui contratos cadastrados");
		}
		return contratos;
	}

	public static Contrato findByNumeroContrato(Integer numeroContrato) throws Exception {
		Contrato contrato = ContratoDAO.findByNumeroContrato(numeroContrato);
		if (contrato == null) {
			throw new Exception("Não foi possível encontrar o contrato com o número " + numeroContrato);
		}
		return contrato;
	}

	private static void validarCampos(Contrato contrato) throws Exception {
		if (contrato.getCliente() == null) {
			throw new Exception("Cliente inexistente");
		}
		ClienteBO.findByCPF(contrato.getCliente().getCpf());
		if (contrato.getNumContrato() == null) {
			throw new Exception("Contrato Inválido");
		}
		if (contrato.getQtdDiasLocacao() == null) {
			throw new Exception("A quantidade de dias de locação não pode ser nulo");
		}
		if (contrato.getDataContrato() == null) {
			throw new Exception("A data do contrato não pode ser nulo");
		}
		if (contrato.getVeiculos().isEmpty()) {
			throw new Exception("Não é possível cadastrar com nenhum veículo");
		}
	}

	private static Double somarValorTotal(Contrato contrato) {
		Double valorTotal = 0D;
		for (Veiculo veiculo : contrato.getVeiculos()) {
			valorTotal += contrato.getQtdDiasLocacao()
					* valorTipoVeiculo(contrato.getDataContrato(), veiculo.getTipo());
		}
		return valorTotal;
	}

	private static Double valorTipoVeiculo(Date date, ETipo tipo) {
		for (TipoVeiculo tipoVeiculo : TipoVeiculoBO.findAll()) {
			if (date.equals(tipoVeiculo.getData()) && tipo.equals(tipoVeiculo.getTipoVeiculo())) {
				return tipoVeiculo.getValor();
			}
		}
		return ETipo.valorTipoVeiculo(tipo);

	}

	public static List<Contrato> findByNumeroOrdenado() throws Exception {
		List<Contrato> contratos = ContratoDAO.findByNumeroOrdenado();
		if (contratos.isEmpty()) {
			throw new Exception("Ainda não possui contratos cadastrados");
		}
		return contratos;
	}

	public static List<Contrato> findByCPF(String cpf) throws Exception {
		List<Contrato> contratos = new ArrayList<Contrato>();
		for (Contrato contrato : ContratoBO.findAll()) {
			if (cpf.equals(contrato.getCliente().getCpf())) {
				contratos.add(contrato);
			}
		}
		if (contratos.isEmpty()) {
			throw new Exception("Não exíste contrato com este CPF");
		}
		return contratos;
	}
}
