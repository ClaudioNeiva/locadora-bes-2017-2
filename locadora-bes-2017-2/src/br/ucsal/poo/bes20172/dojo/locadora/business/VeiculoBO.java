package br.ucsal.poo.bes20172.dojo.locadora.business;

import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Veiculo;
import br.ucsal.poo.bes20172.dojo.locadora.persistence.VeiculoDAO;

public class VeiculoBO {

	static Boolean UPDATE = false;
	static Boolean CREATE = true;

	public static void inserir(Veiculo veiculo) throws Exception {
		validarCampos(veiculo, CREATE);
		VeiculoDAO.insert(veiculo);
	}

	public static void delete(Veiculo veiculo) throws Exception {
		VeiculoDAO.delete(veiculo);
	}

	public static void update(Veiculo veiculo) throws Exception {
		validarCampos(veiculo, UPDATE);
		VeiculoDAO.update(veiculo);
	}

	public static List<Veiculo> findAll() throws Exception {
		List<Veiculo> veiculo = VeiculoDAO.findAll();
		if (veiculo.isEmpty()) {
			throw new Exception("Ainda não possui veículos cadastrados");
		}
		return veiculo;
	}

	public static Veiculo findByPlaca(String placa) throws Exception {
		Veiculo veiculo = VeiculoDAO.findByPlaca(placa);
		if (veiculo == null) {
			throw new Exception("Veículo inexistente");
		}
		return veiculo;
	}

	private static void validarCampos(Veiculo veiculo, Boolean operacao) throws Exception {
		if (veiculo.getAnoFabricacao() == null) {
			throw new Exception("O ano de fabricação não pode ser nulo");
		}
		if (veiculo.getPlaca() == null) {
			throw new Exception("A placa não pode ser nulo");
		}
		validarPlacaRepetida(veiculo, operacao);
		if (veiculo.getTipo() == null) {
			throw new Exception("O tipo não pode ser nulo");
		}
	}

	private static void validarPlacaRepetida(Veiculo veiculo, Boolean operacao) throws Exception {
		List<Veiculo> veiculos = VeiculoDAO.findAll();
		int cont = (operacao) ? 1 : 0; // 0 para UPDATE e 1 para CREATE
		for (Veiculo veiculoLista : veiculos) {
			if (veiculo.getPlaca().equals(veiculoLista.getPlaca())) {
				cont++;
				if (cont == 2)
					throw new Exception("Veículo com placa já existente");
			}
		}
	}

}
