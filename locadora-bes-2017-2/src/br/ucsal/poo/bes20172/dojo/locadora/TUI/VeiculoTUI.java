package br.ucsal.poo.bes20172.dojo.locadora.TUI;

import java.util.List;
import java.util.Scanner;

import br.ucsal.poo.bes20172.dojo.locadora.business.VeiculoBO;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Veiculo;
import br.ucsal.poo.bes20172.dojo.locadora.enums.ETipo;

public class VeiculoTUI {
	private static Scanner sc = new Scanner(System.in);

	public static void menu() {

		Integer opcao;
		do {
			opcao = 0;
			sc = new Scanner(System.in);
			System.out.println("=======================================");
			System.out.println("           Gerenciar Veículos          ");
			System.out.println("=======================================");
			System.out.println("1 - Cadastrar Veículo");
			System.out.println("2 - Remover Veículo");
			System.out.println("3 - Atualizar Veículo");
			System.out.println("4 - Imprimir Veículo");
			System.out.println("5 - Voltar");
			System.out.println("---------------------------------------");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 5) {
				System.out.println(" Opão inválida");
			} else {
				navegarOpcoes(opcao);
			}

		} while (opcao != 5);

	}

	private static void navegarOpcoes(Integer opcao) {
		switch (opcao) {
		case 1:
			try {
				cadastrarVeiculo();
			} catch (Exception e) {
			}
			break;
		case 2:
			try {
				selecionarVeiculoERemover();
			} catch (Exception e) {
			}
			break;
		case 3:
			try {
				selecionarVeiculoEAtualizar();
			} catch (Exception e) {
			}
			break;
		case 4:
			try {
				imprimirVeiculos();
			} catch (Exception e) {
			}
			break;
		default:
			break;
		}
	}

	public static void imprimirVeiculos() throws Exception {
		List<Veiculo> veiculos;
		try {
			veiculos = VeiculoBO.findAll();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception();
		}
		System.out.println("Os veículos Cadastrados são:");
		listarVeiculos(veiculos);
	}

	private static void listarVeiculos(List<Veiculo> veiculos) {
		for (Veiculo veiculo : veiculos) {
			System.out.println("---------------------------------------");
			System.out.println("  Placa: " + veiculo.getPlaca());
			System.out.println("  Ano Fabricação: " + veiculo.getAnoFabricacao());
			System.out.println("  Tipo: " + veiculo.getTipo().getDescricao());
			System.out.println("---------------------------------------");
		}
	}

	private static void selecionarVeiculoEAtualizar() throws Exception {
		imprimirVeiculos();
		Veiculo veiculo = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("=================================================");
			System.out.println(" Selecione um veículo pela placa para atualizar: ");
			System.out.print(" > ");
			String numeroPlaca = sc.nextLine();
			try {
				veiculo = VeiculoBO.findByPlaca(numeroPlaca);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		atualizarVeiculo(veiculo);
	}

	private static void atualizarVeiculo(Veiculo veiculo) {
		Veiculo veiculoAtualizado = camposCadastroVeiculo(veiculo);
		try {
			VeiculoBO.update(veiculoAtualizado);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Atualizado com Sucesso");
	}

	private static void selecionarVeiculoERemover() throws Exception {
		imprimirVeiculos();
		Veiculo veiculo = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("===============================================");
			System.out.println(" Selecione um veículo pela placa para remover: ");
			System.out.print(" > ");
			String numeroPlaca = sc.nextLine();
			try {
				veiculo = VeiculoBO.findByPlaca(numeroPlaca);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		VeiculoBO.delete(veiculo);
		System.out.println("Removido com sucesso");
	}

	private static void cadastrarVeiculo() throws Exception {
		boolean retorno;
		do {
			retorno = false;
			Veiculo veiculo = new Veiculo();
			veiculo = camposCadastroVeiculo(veiculo);
			try {
				VeiculoBO.inserir(veiculo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println("-----------------------------------");
				System.out.println(" Digite novamente as Informações ");
				System.out.println("-----------------------------------");
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		System.out.println("Criado com sucesso");
	}

	private static void continuarOuNao() throws Exception {
		Integer opcao = 0;
		do {
			System.out.println("1 - Continuar");
			System.out.println("2 - Voltar");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 2) {
				System.out.println(" Opão inválida");
				opcao = -1;
			}

		} while (opcao == -1);
		if (opcao == 2) {
			throw new Exception();
		}
	}

	private static Veiculo camposCadastroVeiculo(Veiculo veiculo) {
		sc = new Scanner(System.in);
		System.out.print("Digite a placa: ");
		veiculo.setPlaca(sc.nextLine());
		System.out.print("Digite o Ano de Fabricação: ");
		veiculo.setAnoFabricacao(sc.nextInt());
		System.out.println("Escolha o tipo: ");
		veiculo.setTipo(escolherTipo());
		return veiculo;
	}

	private static ETipo escolherTipo() {
		Integer opcao = 0;
		do {
			for (ETipo tipo : ETipo.values()) {
				System.out.println(tipo.getIndex() + " - " + tipo.getDescricao());
			}
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > ETipo.values().length) {
				System.out.println(" Opão inválida");
				opcao = -1;
			}

		} while (opcao == -1);
		return ETipo.findByIndex(opcao);
	}
}
