package br.ucsal.poo.bes20172.dojo.locadora.main;

import java.util.Scanner;

import br.ucsal.poo.bes20172.dojo.locadora.TUI.ClienteTUI;
import br.ucsal.poo.bes20172.dojo.locadora.TUI.ContratoTUI;
import br.ucsal.poo.bes20172.dojo.locadora.TUI.VeiculoTUI;

public class Main {
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Integer opcao;
		do {
			opcao = 0;
			sc = new Scanner(System.in);
			System.out.println("===============Bem-Vindo===============");
			System.out.println("1 - Gerenciar Clientes ");
			System.out.println("2 - Gerenciar Veículos");
			System.out.println("3 - Gerenciar Contratos");
			System.out.println("4 - Sair");
			System.out.println("---------------------------------------");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 4) {
				System.out.println(" Opão inválida");
			} else {
				navegarOpcoes(opcao);
			}

		} while (opcao != 4);
	}

	private static void navegarOpcoes(Integer opcao) {
		switch (opcao) {
		case 1:
			ClienteTUI.menu();
			break;
		case 2:
			VeiculoTUI.menu();
			break;
		case 3:
			ContratoTUI.menu();
			break;
		default:
			break;
		}
	}

}
