package br.ucsal.poo.bes20172.dojo.locadora.TUI;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import br.ucsal.poo.bes20172.dojo.locadora.business.ClienteBO;
import br.ucsal.poo.bes20172.dojo.locadora.business.ContratoBO;
import br.ucsal.poo.bes20172.dojo.locadora.business.VeiculoBO;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Cliente;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Contrato;
import br.ucsal.poo.bes20172.dojo.locadora.domain.Veiculo;

public class ContratoTUI {

	private static Scanner sc = new Scanner(System.in);
	static Integer CLIENTE = 1;
	static Integer NUMEROCONTRATO = 2;

	public static void menu() {

		Integer opcao;
		do {
			opcao = 0;
			sc = new Scanner(System.in);
			System.out.println("=======================================");
			System.out.println("           Gerenciar Contratos         ");
			System.out.println("=======================================");
			System.out.println("1 - Cadastrar Contrato");
			System.out.println("2 - Atualizar Contrato");
			System.out.println("3 - Imprimir Contratos (Por nome cliente)");
			System.out.println("4 - Imprimir Contratos (Por número contrato)");
			System.out.println("5 - Filtrar por CPF do cliente");
			System.out.println("6 - Voltar");
			System.out.println("---------------------------------------");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 6) {
				System.out.println(" Opão inválida");
			} else {
				navegarOpcoes(opcao);
			}

		} while (opcao != 6);

	}

	private static void navegarOpcoes(Integer opcao) {
		switch (opcao) {
		case 1:
			try {
				cadastrarContrato();
			} catch (Exception e) {
			}
			break;
		case 2:
			try {
				selecionarContratoEAtualizar();
			} catch (Exception e) {
				if (e.getMessage() != null)
					System.out.println(e.getMessage());
			}
			break;
		case 3:
			try {
				imprimirContratoBy(CLIENTE);
			} catch (Exception e) {
			}
			break;
		case 4:
			try {
				imprimirContratoBy(NUMEROCONTRATO);
			} catch (Exception e) {
			}
			break;
		case 5:
			try {
				imprimirFiltrarCPF();
			} catch (Exception e) {
				if (e.getMessage() != null) {
					System.out.println(e.getMessage());
				}
			}
			break;
		default:
			break;
		}
	}

	private static void imprimirFiltrarCPF() throws Exception {
		ClienteTUI.imprimirClientes();
		String cpf = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("=======================================");
			System.out.println(" Digite o cpf do cliente para filtrar  ");
			System.out.print(" > ");
			cpf = sc.nextLine();
			try {
				ClienteBO.findByCPF(cpf);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		listarContratos(ContratoBO.findByCPF(cpf));
	}

	private static void imprimirContratoBy(Integer filtro) throws Exception {
		List<Contrato> contrato = null;
		try {
			if (filtro.equals(CLIENTE)) {
				contrato = ContratoBO.findByClientesOrdenado();
			} else if (filtro.equals(NUMEROCONTRATO)) {
				contrato = ContratoBO.findByNumeroOrdenado();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception();
		}
		System.out.println("Os contratos cadastrados são:");
		listarContratos(contrato);
	}

	private static void listarContratos(List<Contrato> contratos) {
		for (Contrato contrato : contratos) {
			System.out.println("---------------------------------------");
			System.out.println("  Número do Contrato: " + contrato.getNumContrato());
			System.out.println("  Data do Contrato: " + contrato.getDataContrato());
			System.out.print("  Nome do cliente: " + contrato.getCliente().getNome() + "/ CPF: ");
			System.out.println(contrato.getCliente().getCpf());
			System.out.println("  Dias de Locação: " + contrato.getQtdDiasLocacao());
			System.out.println("  Valor Total: " + contrato.getValorTotal());
			System.out.println("  Quantidade de Veículos: " + contrato.getVeiculos().size());
			System.out.println("---------------------------------------");
		}
	}

	private static void selecionarContratoEAtualizar() throws Exception {
		imprimirContratoBy(NUMEROCONTRATO);
		Contrato contrato = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("=================================================================");
			System.out.println(" Selecione um contrato pelo número do contrato para atualizar: ");
			System.out.print(" > ");
			Integer numeroContrato = sc.nextInt();
			try {
				contrato = ContratoBO.findByNumeroContrato(numeroContrato);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		atualizarContrato(contrato);
	}

	private static void atualizarContrato(Contrato contrato) throws Exception {
		Contrato contratoAtualizado = camposCadastroContrato(contrato);
		try {
			ContratoBO.update(contratoAtualizado);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Atualizado com Sucesso");
	}

	private static void cadastrarContrato() throws Exception {
		boolean retorno;
		do {
			retorno = false;
			Contrato contrato = new Contrato();
			contrato = camposCadastroContrato(contrato);
			try {
				ContratoBO.inserir(contrato);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println("-----------------------------------");
				System.out.println(" Digite novamente as Informações ");
				System.out.println("-----------------------------------");
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		System.out.println("Criado com sucesso");
	}

	private static void continuarOuNao() throws Exception {
		Integer opcao = 0;
		do {
			System.out.println("1 - Continuar");
			System.out.println("2 - Voltar");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 2) {
				System.out.println(" Opão inválida");
				opcao = -1;
			}

		} while (opcao == -1);
		if (opcao == 2) {
			throw new Exception();
		}
	}

	private static Contrato camposCadastroContrato(Contrato contrato) throws Exception {
		sc = new Scanner(System.in);
		System.out.print("Digite o número do contrato: ");
		contrato.setNumContrato(sc.nextInt());
		contrato.setDataContrato(new Date());
		System.out.print("Selecione o Cliente: ");
		contrato.setCliente(selecionarCliente());
		System.out.println("Selecione os Veículos: ");
		contrato.setVeiculos(selecioneOsVeículos());
		System.out.println("Digite os dias de Locação: ");
		contrato.setQtdDiasLocacao(sc.nextInt());
		return contrato;
	}

	private static List<Veiculo> selecioneOsVeículos() throws Exception {
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		Veiculo veiculo;
		VeiculoTUI.imprimirVeiculos();
		do {
			sc = new Scanner(System.in);
			System.out.println("==============================================================");
			System.out.println(" Selecione um veículo pela placa para cadastrar no contrato: ");
			System.out.print(" > ");
			String numeroPlaca = sc.nextLine();
			try {
				veiculo = VeiculoBO.findByPlaca(numeroPlaca);
				if (veiculos.isEmpty() || !veiculos.contains(veiculo)) {
					veiculos.add(veiculo);
					System.out.println("Veículo inserido");
				} else if (veiculos.contains(veiculo)) {
					System.out.println("Este veículo já foi inserido");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (cadastrarMaisVeiculos() == 1);
		return veiculos;
	}

	private static int cadastrarMaisVeiculos() {
		Integer opcao = 0;
		do {
			System.out.println("1 - Continuar Inserindo");
			System.out.println("2 - Não inserir mais");
			System.out.print(" > ");
			try {
				opcao = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Atenção digite apenas um número para selecionar a opção\n(Não é permitido letras)");
				sc = new Scanner(System.in);
			}
			System.out.println("=======================================");
			if (opcao <= 0 || opcao > 2) {
				System.out.println(" Opão inválida");
				opcao = -1;
			}

		} while (opcao == -1);
		return opcao;
	}

	private static Cliente selecionarCliente() throws Exception {
		ClienteTUI.imprimirClientes();
		Cliente cliente = null;
		boolean retorno;
		do {
			sc = new Scanner(System.in);
			retorno = false;
			System.out.println("=============================================================");
			System.out.println(" Selecione um cliente pelo CPF para cadastrar no contrato: ");
			System.out.print(" > ");
			String cpf = sc.nextLine();
			try {
				cliente = ClienteBO.findByCPF(cpf);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continuarOuNao();
				retorno = true;
			}
		} while (retorno);
		return cliente;
	}

}
