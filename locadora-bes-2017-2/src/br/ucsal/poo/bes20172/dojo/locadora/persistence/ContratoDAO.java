package br.ucsal.poo.bes20172.dojo.locadora.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Contrato;

public class ContratoDAO {
	private static List<Contrato> contratos = new ArrayList<Contrato>();

	public static List<Contrato> findAll() {
		return contratos;
	}

	public static void insert(Contrato contrato) {
		contratos.add(contrato);
	}

	public static void delete(Contrato contrato) {
		contratos.remove(contrato);
	}

	public static void update(Contrato contrato) {
		/** Atualiza Automaticamente por contada Instância **/
	}

	public static Contrato findByNumeroContrato(Integer numeroContrato) {
		for (Contrato contrato : contratos) {
			if (contrato.getNumContrato().equals(numeroContrato)) {
				return contrato;
			}
		}
		return null;
	}

	public static List<Contrato> findByClientesOrdenado() {
		Collections.sort(contratos);
		return contratos;
	}

	public static List<Contrato> findByNumeroOrdenado() {
		Collections.sort(contratos, new Comparator<Contrato>() {
			@Override
			public int compare(Contrato obj0, Contrato obj1) {
				return obj0.getNumContrato().compareTo(obj1.getNumContrato());
			}
		});
		return contratos;
	}
}
