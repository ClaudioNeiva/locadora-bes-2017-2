package br.ucsal.poo.bes20172.dojo.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.bes20172.dojo.locadora.domain.Cliente;

public class ClienteDAO {
	private static List<Cliente> clientes = new ArrayList<Cliente>();

	public static List<Cliente> findAll() {
		return clientes;
	}

	public static void insert(Cliente cliente) {
		clientes.add(cliente);
	}

	public static void delete(Cliente cliente) {
		clientes.remove(cliente);
	}

	public static void update(Cliente cliente) {
		/**Atualiza Automaticamente por contada Instância**/
	}

	public static Cliente findByCPF(String cpf) {
		for (Cliente cliente : clientes) {
			if (cliente.getCpf().equals(cpf)) {
				return cliente;
			}
		}
		return null;
	}
}
