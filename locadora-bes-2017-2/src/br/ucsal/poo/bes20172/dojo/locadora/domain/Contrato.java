package br.ucsal.poo.bes20172.dojo.locadora.domain;

import java.util.Date;
import java.util.List;

public class Contrato implements Comparable<Contrato> {

	private Integer numContrato;
	private Cliente cliente;
	private List<Veiculo> veiculos;
	private Integer qtdDiasLocacao;
	private Double valorTotal;
	private Date dataContrato;

	public Integer getNumContrato() {
		return numContrato;
	}

	public void setNumContrato(Integer numContrato) {
		this.numContrato = numContrato;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public Integer getQtdDiasLocacao() {
		return qtdDiasLocacao;
	}

	public void setQtdDiasLocacao(Integer qtdDiasLocacao) {
		this.qtdDiasLocacao = qtdDiasLocacao;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public Date getDataContrato() {
		return dataContrato;
	}

	public void setDataContrato(Date dataContrato) {
		this.dataContrato = dataContrato;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	@Override
	public int compareTo(Contrato contrato) {
		return contrato.cliente.getNome().compareTo(cliente.getNome());
	}

}
