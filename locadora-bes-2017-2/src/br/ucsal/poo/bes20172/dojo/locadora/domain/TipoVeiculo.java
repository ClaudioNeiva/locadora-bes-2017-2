package br.ucsal.poo.bes20172.dojo.locadora.domain;

import java.util.Date;

import br.ucsal.poo.bes20172.dojo.locadora.enums.ETipo;

public class TipoVeiculo {
	private Date data;
	private ETipo tipoVeiculo;
	private Double valor;
	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public ETipo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(ETipo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
